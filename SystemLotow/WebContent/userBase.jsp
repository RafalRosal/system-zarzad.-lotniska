<%@ page 
language="java" 
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="java.util.*, controller.*, util.*, model.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="resources/css/bootstrap.min.css"
	type="text/css" rel="stylesheet">
<title>Insert title here</title>
</head>
<body>
<h1>Lista Uzytkownikow w Bazie Danych</h1>

<table class="table table-hover" >
	<thead>
		<tr>
			<th>id</th>
			<th>Login</th>
			<th>Password</th>
			<th>Email</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Phone</th>
			<th>Account Role</th>
			<th>#</th>
			<th>#</th>
		</tr>
	</thead>
	<tbody>
	<%
	LoginController lController = new LoginController();
	List<UserModel> userList = lController.getUser();
	int k = 1;
	for(Object tempUser: userList){
		
	%>
		<tr>
			<td><% out.print(k); %></td>
			<td><% out.print(((UserModel) tempUser).getLogin()); %></td>
			<td><% out.print(((UserModel) tempUser).getPassword()); %></td>
			<td><% out.print(((UserModel) tempUser).getEmail()); %></td>
			<td><% out.print(((UserModel) tempUser).getFirstName()); %></td>
			<td><% out.print(((UserModel) tempUser).getLastName()); %></td>
			<td><% out.print(((UserModel) tempUser).getPhone()); %></td>
			<td><% out.print(((UserModel) tempUser).getAccountRole()); %></td>
			<td><a href="editUser<%k;%>.jsp"> Edytuj</a></td>
			<td><a href="deleteUser.jsp">Usun</a></td>
		</tr>
	<%
		k++;
	}
	%>
	</tbody>
</table>

</body>
</html>
<%@ page 
	language="java" 
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="java.util.*, controller.*, util.*, model.*"
%>

<%
if(session.getAttribute("login") ==  null){
	response.sendRedirect("index.jsp");
}
%>

    
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome!</title>
</head>

<body>
<h1>WELCOME!   </h1>
<a href="./LoginServlet?action=logout"> Wyloguj</a>

<table border=1; callpadding=2 cellspacing=0 width="100%">
	<thead>
		<tr>
			<th>#</th>
			<th>Tytul</th>
			<th>Tresc</th>
		</tr>
	</thead>
	<tbody>
	<%
	TaskController tController = new TaskController();
	List<TaskModel> tasksList = tController.getTasksByUserId(1);
	int k = 1;
	for(Object tempTask: tasksList){
		
	%>
		<tr>
			<td><% out.print(k); %></td>
			<td><% out.print(((TaskModel) tempTask).getSubject()); %></td>
			<td><% out.print(((TaskModel) tempTask).getContent()); %></td>
		</tr>
	<%
		k++;
	}
	%>
	</tbody>
</table>

</body>
</html>
package model;

import java.text.DateFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "plane")
public class PlaneModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String namePlane;
	private DateFormat start;
	private String startFrom;
	private String finishFrom;
	private Double costTicket;
	private int sizePlace;
	private int freePlace;

	public PlaneModel(int id, String namePlane, DateFormat start, String startFrom, String finishFrom,
			Double costTicket, int sizePlace, int freePlace) {
		super();
		this.id = id;
		this.namePlane = namePlane;
		this.start = start;
		this.startFrom = startFrom;
		this.finishFrom = finishFrom;
		this.costTicket = costTicket;
		this.sizePlace = sizePlace;
		this.freePlace = freePlace;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNamePlane() {
		return namePlane;
	}

	public void setNamePlane(String namePlane) {
		this.namePlane = namePlane;
	}

	public DateFormat getStart() {
		return start;
	}

	public void setStart(DateFormat start) {
		this.start = start;
	}

	public String getStartFrom() {
		return startFrom;
	}

	public void setStartFrom(String startFrom) {
		this.startFrom = startFrom;
	}

	public String getFinishFrom() {
		return finishFrom;
	}

	public void setFinishFrom(String finishFrom) {
		this.finishFrom = finishFrom;
	}

	public Double getCostTicket() {
		return costTicket;
	}

	public void setCostTicket(Double costTicket) {
		this.costTicket = costTicket;
	}

	public int getSizePlace() {
		return sizePlace;
	}

	public void setSizePlace(int sizePlace) {
		this.sizePlace = sizePlace;
	}

	public int getFreePlace() {
		return freePlace;
	}

	public void setFreePlace(int freePlace) {
		this.freePlace = freePlace;
	}

}

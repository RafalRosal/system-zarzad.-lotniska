package service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import controller.LoginController;
import model.UserModel;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LoginServlet() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		HttpSession httpSession = request.getSession();
		if (action.equalsIgnoreCase("logout")) {
			httpSession.invalidate();
			response.sendRedirect("index.jsp");

		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		LoginController loginC = new LoginController();
		HttpSession httpSession = request.getSession();

		if (action.equalsIgnoreCase("login")) {
			int userId = loginC.checkIfUserExists(request.getParameter("login"), request.getParameter("password"));
			System.out.println(userId);
			if (userId != -1) {
				httpSession.setAttribute("userId", userId);
				httpSession.setAttribute("login", request.getParameter("login"));
				UserModel user = loginC.findUser(userId);
				System.out.println(user.getAccountRole());
				if (user.getAccountRole().equals("ADMIN")) {
					System.out.println("DUPA");
					response.sendRedirect("admin.jsp");
				} else {
					response.sendRedirect("welcome.jsp");
				}
			} else {
				response.sendRedirect("error.jsp");
			}

		} else {
			if (action.equalsIgnoreCase("register")) {
				if (request.getParameter("password").equals(request.getParameter("repeatPassword"))) {
					loginC.addUser(request.getParameter("login"), request.getParameter("password"),
							request.getParameter("email"), request.getParameter("firstName"),
							request.getParameter("lastName"), Double.parseDouble(request.getParameter("phone")));
					response.sendRedirect("index.jsp");
				}
			}

		}
	}

}

package util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import model.UserModel;

public class HibernateUtil {
	private static SessionFactory sf;

	public static Session getSession() {
		if (sf == null) {
			sf = new Configuration().configure().buildSessionFactory();
			fillExampleData();
		}
		return sf.openSession();
	}

	static private void fillExampleData() {
		Session s = getSession();
		Transaction t = s.beginTransaction();

		try {
			UserModel uModel = new UserModel(0, "admin", "admin", "admin@admin.pl", "admin", "admin", 503922849,
					"ADMIN");
			UserModel uModel1 = new UserModel(1, "admin2", "admin2", "admin2@admin.pl", "admin2", "admin2", 444995,
					"USER");

			s.save(uModel);
			s.save(uModel1);
			t.commit();
		} catch (Exception e) {
			if (t != null) {
				t.rollback();
			}
			e.printStackTrace();
		}
		s.close();
	}

}

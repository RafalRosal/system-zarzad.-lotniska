package controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import model.TaskModel;
import util.HibernateUtil;

public class TaskController {
	public List<TaskModel> getTasksByUserId(int userId) {
		Session session = HibernateUtil.getSession();
		List<TaskModel> tasksList = new ArrayList<>();
		try {
			tasksList = session.createQuery("FROM TaskModel WHERE userId = " + userId).list();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return tasksList;
	}
}

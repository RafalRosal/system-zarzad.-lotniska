package controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import model.UserModel;
import util.HibernateUtil;

public class LoginController {

	public LoginController() {
	}

	public int checkIfUserExists(String userName, String password) {
		Session session = HibernateUtil.getSession();
		int userId = -1;

		try {
			List<UserModel> usersList = session.createSQLQuery(
					"SELECT * FROM users" + " WHERE login = '" + userName + "'" + " AND password = '" + password + "'")
					.addEntity(UserModel.class).list();
			if (usersList.size() != 0) {

				userId = usersList.get(0).getId();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		return userId;

	}

	public UserModel findUser(int userId) {
		UserModel user = null;
		Session session = HibernateUtil.getSession();
		System.out.println("WYWOLANA METODA searchUSER");
		try {
			user = (UserModel) session.createSQLQuery("SELECT * FROM users" + " WHERE id = '" + userId + "'")
					.addEntity(UserModel.class).uniqueResult();
			System.out.println(user);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		System.out.println(user);
		return user;

	}

	public void addUser(String login, String password, String email, String firstName, String lastName, Double phone) {
		Session session = HibernateUtil.getSession();
		Transaction transaction = session.beginTransaction();
		try {
			UserModel userM = new UserModel(0, login, password, email, firstName, lastName, phone, "USER");
			session.save(userM);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public List<UserModel> getUser() {
		Session session = HibernateUtil.getSession();
		List<UserModel> userList = new ArrayList<>();
		try {
			userList = session.createQuery("FROM UserModel").list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return userList;
	}

}
